package com.aximus.closednetwork.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.aximus.closednetwork.R;

public class ProgressBarHandler extends ProgressBar {


    private MaterialDialog mProgressDialog;

    public ProgressBarHandler(Context context) {
        super(context);
    }

    public void show(Context context) {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        } else {
            showProgressDialog(context);
        }


    }

    public void hide() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

    }

    public void showProgressDialog(final Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .content(R.string.please_wait)
                .widgetColor(ContextCompat.getColor(context, R.color.colorAccent))
                .progress(true, 0)
                .cancelable(false)
                .canceledOnTouchOutside(false);
        mProgressDialog = builder.build();
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }
}
