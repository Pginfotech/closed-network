package com.aximus.closednetwork.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.signin.LoginModel;
import com.aximus.closednetwork.webservices.resopnse.LoginResponse;

public class CommonSharedPreferences {
    private Context context;
    private SharedPreferences sharedPreference;
    private CommonSharedPreferences commonSharedPreferences;

    private String userName = "";
    private String password = "";
    private String currentLanguage = "";
    private String expDate;
    private String firebaseToken = "";

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public String getCurrentLanguage() {
        return currentLanguage;
    }

    public void setCurrentLanguage(String currentLanguage) {
        this.currentLanguage = currentLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public void setToken(String token) {
        this.firebaseToken = token;
    }

    public CommonSharedPreferences(Context context) {
        this.context = context;
        setUpSharedPreferences();
        setData();
    }

    private void setData() {
        setUserName(sharedPreference.getString("userName", ""));
        setPassword(sharedPreference.getString("password", ""));
        setToken(sharedPreference.getString("firebaseToken", ""));
        setCurrentLanguage(sharedPreference.getString("currentLanguage", "En"));
    }

    public void setUpSharedPreferences() {
        sharedPreference = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
    }

    public void setPreferenceData(final LoginResponse loginModel) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        ClosedNetworkApp.Log("userPassword setPreferenceData :"+ loginModel.userPassword);
        editor.putString("userName", loginModel.userName);
        editor.putString("email", loginModel.userEmaill);
        editor.putInt("userId", loginModel.userId);
        editor.putString("userCountry", loginModel.userCountry);
        editor.putString("userAddress", loginModel.userAddress);
        editor.putString("password", loginModel.userPassword);
        editor.putString("phoneNumber1", loginModel.userPhone1);
        editor.putString("phoneNumber2", loginModel.userPhone2);
        editor.putString("userCity", loginModel.userCity);
        setCurrentLanguage(sharedPreference.getString("currentLanguage", "En"));
        editor.putBoolean(context.getString(R.string.pref_is_login),true);
        editor.apply();
    }

    public LoginResponse getPreferenceData() {
        LoginResponse loginModel = new LoginResponse();
        loginModel.userName = sharedPreference.getString("userName", "");
        loginModel.userEmaill = sharedPreference.getString("email", "");
        loginModel.userId = sharedPreference.getInt("userId", 0);
        loginModel.userCountry = sharedPreference.getString("userCountry", "");
        loginModel.userAddress = sharedPreference.getString("userAddress", "");
        loginModel.userPassword = sharedPreference.getString("password", "");
        loginModel.userPhone1 = sharedPreference.getString("phoneNumber1", "");
        loginModel.userPhone2 = sharedPreference.getString("phoneNumber2", "");
        loginModel.userCity = sharedPreference.getString("userCity", "");
        return loginModel;
    }

    public SharedPreferences getSharedPreference() {
        return sharedPreference;
    }

    public void clearPreferences() {
        if (sharedPreference != null) {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.clear();
            editor.putBoolean(context.getString(R.string.pref_is_login),false);
            editor.putBoolean(context.getString(R.string.pref_intro_view),true);
            editor.apply();
        } else {
            setUpSharedPreferences();
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.clear();
            editor.apply();
        }
    }
}
