package com.aximus.closednetwork.intro;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.dividends.Dividends;
import com.aximus.closednetwork.home.Home;
import com.aximus.closednetwork.signin.SignIn;
import com.aximus.closednetwork.signup.SignUp;

import java.util.Objects;

public class ThreeIntroFragment extends Fragment implements View.OnClickListener {

    LinearLayout layoutSignIn;





    public static Fragment newInstance() {
        return new ThreeIntroFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);

        layoutSignIn = view.findViewById(R.id.layout_Signin);
        layoutSignIn.setOnClickListener(this);

        return view;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        if(v == layoutSignIn){
//            Intent intent = new Intent(getContext(), Home.class);
//            getActivity().startActivity(intent);
            ((IntroActivity) Objects.requireNonNull(getActivity())).setCurrentItem (2, true);
        }
    }






}
