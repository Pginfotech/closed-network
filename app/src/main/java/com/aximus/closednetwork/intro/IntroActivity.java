package com.aximus.closednetwork.intro;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener {


    private ViewPager viewPager;
    private RelativeLayout layoutBottom;
    private ImageView imgIntroFrist;
    private AppCompatTextView txtIntroTwo;
    private AppCompatTextView txtIntroThree;
    private DotsIndicator dotIndicatior;
    Activity act;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        act = this;
//        initializeComponents();
//        addComponent();
        viewPager = findViewById(R.id.view_pager);
        dotIndicatior = findViewById(R.id.dots_indicator);

        IntroPagerAdapter adapterViewPager = new IntroPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        dotIndicatior.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                setCurrentItem(position,true);
//                if(position ==1){
//                    imgIntroFrist.setVisibility(View.VISIBLE);
//                    txtIntroTwo.setVisibility(View.GONE);
//                    txtIntroThree.setVisibility(View.GONE);
//                }
//
//              else   if(position ==2){
//                    txtIntroTwo.setVisibility(View.VISIBLE);
//                    txtIntroThree.setVisibility(View.GONE);
//                    imgIntroFrist.setVisibility(View.GONE);
//                }else {
//                    txtIntroThree.setVisibility(View.VISIBLE);
//                    txtIntroTwo.setVisibility(View.GONE);
//                    imgIntroFrist.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

//    @Override
//    public void initializeComponents() {
//        imgIntroFrist = findViewById(R.id.img_next);
//        txtIntroTwo = findViewById(R.id.txt_sigin);
//        txtIntroThree = findViewById(R.id.txt_center_getstarted);
//        layoutBottom = findViewById(R.id.layout_bottom);
//        viewPager = findViewById(R.id.view_pager);
//        dotIndicatior = findViewById(R.id.dots_indicator);

//        imgIntroFrist.setOnClickListener(this);
//        txtIntroTwo.setOnClickListener(this);
//        txtIntroThree.setOnClickListener(this);


//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//    @Override
//    public void addComponent() {
//
//    }

    @Override
    public void onClick(View view) {
//        if(view.getId() == txtSkip.getId()){
//            openSignUpScreen();
//        }else if(view.getId() == txtGetStarted.getId()){
//            openSignUpScreen();
//        }else if(view.getId() == txtCenterGetStarted.getId()){
//            openSignUpScreen();
//        }
    }
    private void openSignUpScreen(){
//        Intent intent = new Intent(this, LoginActivity.class);
//        startActivity(intent);
//        finish();
    }

    public void setCurrentItem (int item, boolean smoothScroll) {
        viewPager.setCurrentItem(item, smoothScroll);
    }
}
