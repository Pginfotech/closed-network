package com.aximus.closednetwork.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.contactus.ContactUs;
import com.aximus.closednetwork.home.Home;
import com.aximus.closednetwork.signin.SignIn;
import com.aximus.closednetwork.signup.SignUp;

public class TwoIntroFragment extends Fragment implements View.OnClickListener {

    LinearLayout layoutSignIn;
    AppCompatTextView txtSignUp;


    public static Fragment newInstance() {
        return new TwoIntroFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        layoutSignIn = view.findViewById(R.id.layout_Signin);
        txtSignUp = view.findViewById(R.id.txt_SignUp);
        layoutSignIn.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == layoutSignIn){
            Intent intent = new Intent(getContext(), SignIn.class);
            getActivity().startActivity(intent);
        }
        else if(v == txtSignUp){
            Intent intent = new Intent(getContext(), SignUp.class);
            getActivity().startActivity(intent);
        }

    }
}
