package com.aximus.closednetwork.intro;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.home.Home;

import java.util.Objects;

public class OneIntroFragment extends Fragment implements View.OnClickListener {

    ImageView imgNext;
    ViewPager viewPager;




    public static Fragment newInstance() {
        return new OneIntroFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frist, container, false);

        imgNext = view.findViewById(R.id.img_Next);
        imgNext.setOnClickListener(this);
//        viewPager = view.findViewById(R.id.view_pager);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        if(v == imgNext){
//            viewPager.setCurrentItem(getItem(+1), true); //getItem(-1) for previous

            ((IntroActivity) Objects.requireNonNull(getActivity())).setCurrentItem (1, true);


        }
    }
}
