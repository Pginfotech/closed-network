package com.aximus.closednetwork.report;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.setting.Setting;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Report extends AppCompatActivity implements View.OnClickListener, OnChartValueSelectedListener {

    ImageView imgReportSetting;
    private CombinedChart mChart;
    Activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_report);
        act = this;

        imgReportSetting = findViewById(R.id.img_ReportSetting);
        imgReportSetting.setOnClickListener(this);

        CreateChart();



    }

    @Override
    public void onClick(View v) {
          if(v == imgReportSetting){
            Intent intent = new Intent(act, Setting.class);
            act.startActivity(intent);
        }
        }

private void CreateChart(){
    mChart = findViewById(R.id.chart1);
//    mChart.getDescription().setText("$4153.93 \\n15TH MAY - 15TH JUNE");
    mChart.getDescription().setText("$4153.93 15TH MAY - 15TH JUNE");
    mChart.getDescription().setTextColor(getResources().getColor(R.color.color_white));
//    mChart.getDescription().setTextSize(10f);

//    mChart.getDescription().setPosition(3f,3f);
//        mChart.setBackgroundColor(getResources().getColor(R.color.appbg));
//    mChart.setBackgroundResource(R.drawable.app_rounded_gradient);
//        mChart.setGridBackgroundColor();

//        mChart.setDrawGridBackground(true);
//        mChart.setDrawBarShadow(true);
//        mChart.setHighlightFullBarEnabled(true);

    // create marker to display box when values are selected
    mChart.setTouchEnabled(true);
    mChart.setOnChartValueSelectedListener(this);
    MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

    // Set the marker to the chart
    mv.setChartView(mChart);
    mChart.setMarker(mv);


//    mChart.getDescription().setEnabled(false);
    mChart.getAxisLeft().setDrawGridLines(false);
    mChart.getXAxis().setDrawGridLines(false);
    mChart.getXAxis().setDrawAxisLine(false);
    mChart.setDragEnabled(true);
    mChart.setScaleEnabled(true);
    mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    mChart.getAxisLeft().setEnabled(false);
    mChart.getAxisRight().setEnabled(false);
    mChart.getLegend().setEnabled(false);
//        mChart.getXAxis().setAvoidFirstLastClipping(false);
//        mChart.setVisibleXRangeMaximum(5f);
//        mChart.setViewPortOffsets(0f, 0f, 0f, 0f);

    mChart.setMaxHighlightDistance(6f);

    LimitLine ll = new LimitLine(100, "Max Capacity");
    mChart.getAxisLeft().addLimitLine(ll);
    mChart.setPinchZoom(true);
    mChart.setDrawBarShadow(true);
//        mChart.moveViewToX(10);


    // draw bars behind lines
    mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
            CombinedChart.DrawOrder.LINE
    });
//        CombinedChart.DrawOrder.BAR,
    Legend l = mChart.getLegend();
    l.setWordWrapEnabled(true);
    l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
    l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
    l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
    l.setForm(Legend.LegendForm.LINE);

    YAxis rightAxis = mChart.getAxisRight();
    rightAxis.setDrawGridLines(false);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)




    YAxis leftAxis = mChart.getAxisLeft();
    leftAxis.setDrawGridLines(false);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
    leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setAxisMinimum(-4);
//        leftAxis.setSpaceMax(5f);


    leftAxis.setDrawAxisLine(false);
//        leftAxis.setDrawZeroLine(false);
//        leftAxis.setDrawGridLines(false);

    XAxis xAxis = mChart.getXAxis();
    xAxis.setPosition(XAxis.XAxisPosition.TOP);
//        xAxis.setAxisMinimum(0f);

    xAxis.setGranularityEnabled(true);

//        xAxis.setSpaceMin(150f);
//        xAxis.setGranularity(1.0f);
//    xAxis.setTypeface(AppController.tazRegular);
    xAxis.setTextSize(9f);
    xAxis.setTextColor(getResources().getColor(R.color.color_white));

    xAxis.setValueFormatter(new ValueFormatter() {
        @Override
        public String getFormattedValue(float value) {
            return mMonths[(int) value % mMonths.length];
        }
    });


    CombinedData data = new CombinedData();


    data.setData(generateLineData());
//     xAxis.setSpaceMax(35f);
//     xAxis.setSpaceMin(5f);
//        data.setData(generateBarData());

//    xAxis.setAxisMaximum(data.getXMax() + 0.25f);
//    mChart.getXAxis().setAxisMaximum(data.getXMax());
//    mChart.getXAxis().setAxisMinimum(data.getXMin());
    mChart.setViewPortOffsets(18f, 35f, 18f, 35f);
//        xAxis.setAxisMaximum(data.getXMax() + 1f);
//        xAxis.setLabelCount(7);
//        xAxis.setGranularity(0.1f);
    mChart.setData(data);
    mChart.invalidate();
}

    private LineData generateLineData() {


        ArrayList<Entry> entries = new ArrayList<Entry>();
        ArrayList<Entry> entries2 = new ArrayList<Entry>();

        entries = getLineEntriesData(entries);
        entries2 = getLineEntriesDataTwo(entries2);


        LineDataSet set = new LineDataSet(entries, "Line");
        set.setValueFormatter(new PercentFormatter());
        set.setDrawIcons(false);
        set.setColor(getResources().getColor(R.color.color_white));
        set.setLineWidth(4f);
        set.setCircleRadius(4f);
        set.setDrawCircles(true);

        set.setCircleColor(Color.rgb(85, 130, 245));
        set.setCircleRadius(4f);
        set.setFillColor(Color.rgb(85, 130, 245));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(false);
        set.setValueTextSize(10f);
        set.setDrawIcons(true);
//        set.setValueTextColor(getResources().getColor(R.color.color_white));
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setCubicIntensity(0.2f);
//        set.setDrawFilled(true);LineData
//        set.setFillAlpha(100);
//        set.setDrawHorizontalHighlightIndicator(false);

        LineDataSet set2 = new LineDataSet(entries2, "Line");
        set2.setColor(Color.rgb(93, 172, 249));


//        set2.setColors(ColorTemplate.COLORFUL_COLORS);
        set2.setDrawCircles(false);
        set2.setLineWidth(0f);
        set2.setCircleColor(Color.rgb(240, 238, 70));
        set2.setCircleRadius(5f);
        set2.setFillColor(Color.rgb(240, 238, 70));
        set2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set2.setDrawValues(false);
        set2.setValueTextSize(10f);
        set2.setValueTextColor(Color.rgb(240, 238, 70));


        set2.setAxisDependency(YAxis.AxisDependency.LEFT);
        LineData d = new LineData(set,set2);

        int maxCapacity = 100;


        return d;


    }

    protected String[] mMonths = new String[]{
            "JAN", "FEB", "MAR", "APR", "MAY","JUN"
//            ,
//            "June","Jul","Aug"
    };



    private ArrayList<Entry> getLineEntriesData(ArrayList<Entry> entries2) {


//        entries2.add(new Entry(0, 20f));
//        entries2.add(new Entry(1, 60f));
//        entries2.add(new Entry(2, 30f));
//        entries2.add(new Entry(3, 10f));
//        entries2.add(new Entry(4, 80f));
//        entries2.add(new Entry(5, 30f));


        entries2.add(new Entry(0, 30f));
        entries2.add(new Entry(1, 60));
        entries2.add(new Entry(2, 10));
        entries2.add(new Entry(3, 8));
        entries2.add(new Entry(4, 40));
        entries2.add(new Entry(5, 37));
//        entries2.add(new Entry(6, 15f));
//        entries2.add(new Entry(7, 8f));

        return entries2;
    }

    private ArrayList<Entry> getLineEntriesDataTwo(ArrayList<Entry> entries) {
        entries.add(new Entry(0, 100f));
        entries.add(new Entry(1, 100f));
        entries.add(new Entry(2, 100f));
        entries.add(new Entry(3, 100f));
        entries.add(new Entry(4, 100f));
        entries.add(new Entry(5, 100f));
//        entries.add(new Entry(6, 0));
//        entries.add(new Entry(7, 0));

        return entries;
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
