package com.aximus.closednetwork;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.WindowManager;

import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate;
import com.aximus.closednetwork.webservices.APIClient;
import com.github.mikephil.charting.utils.Utils;

public class ClosedNetworkApp extends Application {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    LocalizationApplicationDelegate localizationDelegate = new LocalizationApplicationDelegate(this);
    public static final String TAG = ClosedNetworkApp.class
            .getSimpleName();
    public SharedPreferences preferences;
    public static ProgressDialog progressDialog;
    private static ClosedNetworkApp mInstance;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localizationDelegate.onConfigurationChanged(this);


    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }


    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        mInstance = this;
        //Stetho.initializeWithDefaults(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


    }
    public static void Log(Object msg) {
        Log.e("ClosedNetworkApp : ==> ", msg + "");
    }


    public static APIClient getApiClient() {
        final APIClient apiClient = new APIClient();
        return apiClient;
    }
    public static void showProgress(Activity act, String strMsg) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new ProgressDialog(act, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(strMsg);
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Showing Alert Message
                try {
                    if (progressDialog != null && !progressDialog.isShowing())
                        progressDialog.show();
                } catch (WindowManager.BadTokenException e) {
                    ClosedNetworkApp.Log(e.toString());
                }
            }
        });
    }

    public static void dismissProgress() {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        } catch (IllegalArgumentException e) {

        }
    }

}
