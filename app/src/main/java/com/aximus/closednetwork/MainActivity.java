package com.aximus.closednetwork;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.aximus.closednetwork.intro.IntroActivity;


public class MainActivity extends AppCompatActivity {

    private final static int SPLASH_TIME_OUT = 3000;
    private Handler myHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        myHandler = new Handler();
        myHandler.postDelayed(myRunnable, SPLASH_TIME_OUT);
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
//
//            if (preferences.getBoolean(getString(R.string.pref_intro_view), false)) {
//                if (preferences.getBoolean(getString(R.string.pref_is_login), false)) {
//                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
//                    startActivity(i);
//                    finish();
//                } else {
//                    Intent i = new Intent(SplashActivity.this, IntroLanguageActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//            } else {
//
//                SharedPreferences.Editor editor = preferences.edit();
//                editor.putBoolean(getString(R.string.pref_intro_view),true);
//                editor.commit();
//
//                Intent i = new Intent(SplashActivity.this, IntroLanguageActivity.class);
//                startActivity(i);
//                finish();
//            }
            Intent i = new Intent(MainActivity.this, IntroActivity.class);
                startActivity(i);
                finish();

        }
    };
    @Override
    public void onBackPressed() {
        if (myHandler != null && myRunnable != null) {
            myHandler.removeCallbacks(myRunnable);
        }
        super.onBackPressed();
    }
}
