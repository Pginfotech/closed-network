package com.aximus.closednetwork.home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Network;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.changpwd.ChangedPassword;
import com.aximus.closednetwork.common.Constant;
import com.aximus.closednetwork.report.MyMarkerView;
import com.aximus.closednetwork.report.Report;
import com.aximus.closednetwork.setting.Setting;
import com.aximus.closednetwork.tab_bottom.SpaceItem;
import com.aximus.closednetwork.tab_bottom.SpaceNavigationView;
import com.aximus.closednetwork.tab_bottom.SpaceOnClickListener;
import com.aximus.closednetwork.tab_bottom.SpaceOnLongClickListener;
import com.aximus.closednetwork.view.DividerItemDecoration;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class Home extends BaseActivity implements View.OnClickListener, OnChartValueSelectedListener,ArticlesContract.ArticlesView  {
//        implements View.OnClickListener {

    private SpaceNavigationView spaceNavigationView;
    Activity act;
    DividerItemDecoration decoration;
    HomeListAdapater homeListAdapter;
    RecyclerView rvHomelist;
    public  List<HomeListObject> arrthomeList = new ArrayList<HomeListObject>();
    ScrollView layoutscrollPrfoile,layoutscrollDividends;
    LinearLayout layoutHome,layoutReport,layoutReportScroll;
    AppCompatTextView txtChangedPwd;
    private CombinedChart mChartDiviends;
    ImageView imgDiviendsSetting,imgProfileSetting,imgReportSetting;
    private CombinedChart mChart;
    LineChart mcaherLine;

    protected final String[] months = new String[] {
            "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"
    };

    CoordinatorLayout layoutProfile;
    ArticlesPresenterImpl articlesPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tab_home);
        act = this;

        articlesPresenter = new ArticlesPresenterImpl(Home.this);
        articlesPresenter.onAttach(this);
        layoutHome = findViewById(R.id.layout_Home);
        rvHomelist = findViewById(R.id.rv_Homelst);



        txtChangedPwd = findViewById(R.id.txt_ChangPwd);
        txtChangedPwd.setOnClickListener(this);
        imgProfileSetting = findViewById(R.id.img_Profile_Setting);
        imgProfileSetting.setOnClickListener(this);
        layoutscrollPrfoile = findViewById(R.id.scroll_Profile);
//        layoutProfile = findViewById(R.id.layout_Profile);


        layoutscrollDividends = findViewById(R.id.scroll_Dividends);
        mChartDiviends = findViewById(R.id.chart_Dividens);
        imgDiviendsSetting = findViewById(R.id.img_Dividens_Setting);
        imgDiviendsSetting.setOnClickListener(this);


        layoutReport = findViewById(R.id.layout_Report);
        layoutReportScroll = findViewById(R.id.layout_Scroll_Report);
        imgReportSetting = findViewById(R.id.img_ReportSetting);
        imgReportSetting.setOnClickListener(this);







//        layoutHome();
        spaceNavigationView = findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem(R.id.navigation_first,"Home", R.drawable.ic_tab_home_vector));
        spaceNavigationView.addSpaceItem(new SpaceItem(R.id.navigation_second, "Projects", R.drawable.ic_tab_project_vector));
        spaceNavigationView.addSpaceItem(new SpaceItem(R.id.navigation_third, "Dividends", R.drawable.ic_tab_diviends_vector));
        spaceNavigationView.addSpaceItem(new SpaceItem(R.id.navigation_forth, "Account", R.drawable.ic_tab_account_vector));
        spaceNavigationView.shouldShowFullBadgeText(true);

        spaceNavigationView.setCentreButtonId(R.id.navigation_centre);
        spaceNavigationView.setCentreButtonIconColorFilterEnabled(false);

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Log.d("onCentreButtonClick ", "onCentreButtonClick");
                layoutHome.setVisibility(View.GONE);
                rvHomelist.setVisibility(View.GONE);
                layoutscrollDividends.setVisibility(View.GONE);
//                    layoutProfile.setVisibility(View.GONE);
                layoutscrollPrfoile.setVisibility(View.GONE);


                layoutReport.setVisibility(View.VISIBLE);
                layoutReportScroll.setVisibility(View.VISIBLE);
                layoutReport();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                Log.e("onItemClick ", "" + itemIndex + " " + itemName);

//                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                if(itemIndex == 0) {

                    layoutHome.setVisibility(View.VISIBLE);
                    rvHomelist.setVisibility(View.VISIBLE);
                    layoutscrollDividends.setVisibility(View.GONE);
//                    layoutProfile.setVisibility(View.GONE);
                    layoutscrollPrfoile.setVisibility(View.GONE);
                    layoutReport.setVisibility(View.GONE);
                    layoutReportScroll.setVisibility(View.GONE);

                }
                if(itemIndex == 1) {
                    layoutscrollDividends.setVisibility(View.GONE);
                    layoutHome.setVisibility(View.GONE);
                    rvHomelist.setVisibility(View.GONE);
//                    layoutProfile.setVisibility(View.GONE);
                    layoutscrollPrfoile.setVisibility(View.GONE);
                    layoutReport.setVisibility(View.GONE);
                    layoutReportScroll.setVisibility(View.GONE);

                }
                if(itemIndex == 2) {
                    layoutscrollDividends.setVisibility(View.VISIBLE);
                    layoutHome.setVisibility(View.GONE);
                    rvHomelist.setVisibility(View.GONE);
//                    layoutProfile.setVisibility(View.GONE);
                    layoutscrollPrfoile.setVisibility(View.GONE);
                    layoutReport.setVisibility(View.GONE);
                    layoutReportScroll.setVisibility(View.GONE);


                    layoutDividends();

                }

                if(itemIndex == 3) {
//                    layoutProfile.setVisibility(View.VISIBLE);
                    layoutscrollPrfoile.setVisibility(View.VISIBLE);
                    layoutscrollDividends.setVisibility(View.GONE);
                    layoutHome.setVisibility(View.GONE);
                    rvHomelist.setVisibility(View.GONE);
                    layoutReport.setVisibility(View.GONE);
                    layoutReportScroll.setVisibility(View.GONE);


                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                Log.d("onItemReselected ", "" + itemIndex + " " + itemName);
            }
        });


//        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
//            @Override
//            public void onCentreButtonClick() {
//                layoutHome.setVisibility(View.GONE);
//                rvHomelist.setVisibility(View.GONE);
//                layoutscrollDividends.setVisibility(View.GONE);
////                    layoutProfile.setVisibility(View.GONE);
//                layoutscrollPrfoile.setVisibility(View.GONE);
//
//
//                layoutReport.setVisibility(View.VISIBLE);
//                layoutReportScroll.setVisibility(View.VISIBLE);
//                layoutReport();
//            }
//
//            @Override
//            public void onItemClick(int itemIndex, String itemName) {
//
//            }
//
//            @Override
//            public void onItemReselected(int itemIndex, String itemName) {
//
//            }
//        });
//        spaceNavigationView.setSpaceOnLongClickListener(new SpaceOnLongClickListener() {
//            @Override
//            public void onCentreButtonLongClick() {
//                Toast.makeText(act, "onCentreButtonLongClick", Toast.LENGTH_SHORT).show();
//                layoutHome.setVisibility(View.GONE);
//                rvHomelist.setVisibility(View.GONE);
//                layoutscrollDividends.setVisibility(View.GONE);
////                    layoutProfile.setVisibility(View.GONE);
//                layoutscrollPrfoile.setVisibility(View.GONE);
//
//
//                layoutReport.setVisibility(View.VISIBLE);
//                layoutReportScroll.setVisibility(View.VISIBLE);
//                layoutReport();
////                Intent intent = new Intent(act, Report.class);
////                act.startActivity(intent);
//
//            }
//
//            @Override
//            public void onItemLongClick(int itemIndex, String itemName) {
//                Toast.makeText(act, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
//            }
//        });
////        spaceNavigationView.showIconOnly();

        initializeComponents();
        addComponent();

        articlesPresenter.callWebService();

//
    }

    @Override
    public void initializeComponents() {

    }

    @Override
    public void addComponent() {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }

//    private void layoutHome() {
//
//
//         decoration = new DividerItemDecoration(Constant.dipToPx(act, 4), 2);
//        rvHomelist.addItemDecoration(decoration);
//                rvHomelist.setHasFixedSize(true);
//
//        homeListAdapter = new HomeListAdapater(arrthomeList,act,2);
//        rvHomelist.setAdapter(homeListAdapter);
////        rvHomelist.addOnScrollListener(getOnBottomListener((StaggeredGridLayoutManager) rvHomelist.getLayoutManager()));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//
//
//        StaggeredGridLayoutManager Layoutmanager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
//        Layoutmanager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
//        rvHomelist.setLayoutManager(Layoutmanager);
//
//    }
//    RecyclerView.OnScrollListener getOnBottomListener(final StaggeredGridLayoutManager layoutManager) {
//        return new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView rv, int dx, int dy) {
//                Log.e("Tagg,,,,,4 ","lll");
//                if (arrthomeList != null && arrthomeList.size() > 0) {
//                    boolean isBottom = layoutManager.findLastVisibleItemPositions(new int[3])[1] >= homeListAdapter.getItemCount() - 3;
//                   Log.e("Tag ;;",""+(layoutManager.findLastCompletelyVisibleItemPositions(new int[3])[1]) + " : " + (homeListAdapter.getItemCount() - 3));
//                    if (isBottom) {
//                        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//                        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//                        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//                        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//
//                        homeListAdapter = new HomeListAdapater(arrthomeList,act,2);
//                        rvHomelist.setAdapter(homeListAdapter);
//                    }
//                }
//            }
//        };
//    }

    private void layoutProfile(){
        layoutHome.setVisibility(View.GONE);
        rvHomelist.setVisibility(View.GONE);
        layoutscrollPrfoile.setVisibility(View.VISIBLE);

    }

    private void layoutDividends(){
        mChartDiviends.setEnabled(false);
        mChartDiviends.setTouchEnabled(false);

        mChartDiviends.getDescription().setText("");
        mChartDiviends.setBackgroundColor(getResources().getColor(R.color.app_theme_color));
//        mChartDiviends.setDrawGridBackground(true);
//        mChartDiviends.setDrawBarShadow(true);
        mChartDiviends.setHighlightFullBarEnabled(false);


        mChartDiviends.getAxisLeft().setDrawGridLines(false);
        mChartDiviends.getXAxis().setDrawGridLines(false);
        mChartDiviends.getAxisLeft().setDrawLabels(false);
        mChartDiviends.getAxisRight().setDrawLabels(false);

        mChartDiviends.getDescription().setEnabled(false);

        // draw bars behind lines
//        mChartDiviends.setDrawOrder(new CombinedChart.DrawOrder[]{
//                CombinedChart.DrawOrder.BAR,  CombinedChart.DrawOrder.LINE
//        });
        mChartDiviends.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.BAR
        });

        Legend l = mChartDiviends.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        mChartDiviends.getLegend().setEnabled(false);



        YAxis rightAxis = mChartDiviends.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setEnabled(false);

        YAxis leftAxis = mChartDiviends.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setEnabled(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        leftAxis.setEnabled(false);



//        ArrayList<String> xAxisLabel = new ArrayList<>();
//        xAxisLabel.add("JAN");
//        xAxisLabel.add("FEB");
//        xAxisLabel.add("MAR");
//        xAxisLabel.add("APR");
//        xAxisLabel.add("MAY");
//        xAxisLabel.add("JUN");
//        xAxisLabel.add("JUL");

        XAxis xAxis = mChartDiviends.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(getResources().getColor(R.color.color_white));

        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return months[(int) value % months.length];
            }
        });
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return mMonths[(int) value % mMonths.length];
//            }
//        });

        CombinedData data = new CombinedData();

//        data.setData( generateLineData());
        data.setData(generateBarData());

//        xAxis.setAxisMaximum(data.getXMax() + 0.25f);
//        xAxis.setAxisMinimum(-0.5f);

        mChartDiviends.getXAxis().setAxisMaximum(data.getXMax() + 0.25f);
        mChartDiviends.getXAxis().setAxisMinimum(data.getXMin() - 0.25f);


        mChartDiviends.setData(data);
        mChartDiviends.invalidate();

    }
    private BarData generateBarData() {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        entries = getBarEnteries(entries);

        BarDataSet set1 = new BarDataSet(entries, "");
        //set1.setColor(Color.rgb(60, 220, 78));
//        Paint mPaint = mChartDiviends.getRenderer().getPaintRender(); mPaint.setShader(new
//                SweepGradient(350,120,Color.parseColor("#FFF212"),Color.parseColor("#FCE121")));

//        int startColor1 = ContextCompat.getColor(this, android.R.color.holo_orange_light);
//        int startColor2 = ContextCompat.getColor(this, android.R.color.holo_blue_light);
//        int startColor3 = ContextCompat.getColor(this, android.R.color.holo_orange_light);
//        int startColor4 = ContextCompat.getColor(this, android.R.color.holo_green_light);
//        int startColor5 = ContextCompat.getColor(this, android.R.color.holo_red_light);
//        int endColor1 = ContextCompat.getColor(this, android.R.color.holo_blue_dark);
//        int endColor2 = ContextCompat.getColor(this, android.R.color.holo_purple);
//        int endColor3 = ContextCompat.getColor(this, android.R.color.holo_green_dark);
//        int endColor4 = ContextCompat.getColor(this, android.R.color.holo_red_dark);
//        int endColor5 = ContextCompat.getColor(this, android.R.color.holo_orange_dark);
//
//        List<GradientColor> gradientColors = new ArrayList<>();
//        gradientColors.add(new GradientColor(startColor1, endColor1));
//        gradientColors.add(new GradientColor(startColor2, endColor2));
//        gradientColors.add(new GradientColor(startColor3, endColor3));
//        gradientColors.add(new GradientColor(startColor4, endColor4));
//        gradientColors.add(new GradientColor(startColor5, endColor5));
//        gradientColors.add(new GradientColor(startColor2, endColor2));
//
//        set1.setGradientColors(gradientColors);

//        set1.setBarBorderWidth(0.11f);
        set1.setDrawIcons(false);
        set1.setColors(ColorTemplate.JOYFUL_COLORS);
        set1.setValueTextColor(getResources().getColor(R.color.color_white));
        set1.setValueTextSize(10f);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//        set1.setBarBorderWidth(50f);
        set1.getEntryForIndex(3).setIcon(ContextCompat.getDrawable(this,R.drawable.ic_arrow_left));

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();

        dataSets.add(set1);
//
        float barWidth = 0.12f; // x2 dataset

//
//        BarData d = new BarData(set1);
//        d.setBarWidth(barWidth);
////        d.setBarWidth(0.9f);



        BarData d = new BarData(set1);
        d.setBarWidth(barWidth);


        // make this BarData object grouped
//        d.groupBars(0, groupSpace, barSpace); // start at x = 0




        return d;
    }

    private ArrayList<BarEntry> getBarEnteries(ArrayList<BarEntry> entries){
        entries.add(new BarEntry(0, 13));
        entries.add(new BarEntry(1, 25));
        entries.add(new BarEntry(2, 30));
        entries.add(new BarEntry(3, 38));
        entries.add(new BarEntry(4, 10));
        entries.add(new BarEntry(5, 15));
        entries.add(new BarEntry(6, 20));
        return  entries;
    }
    private void layoutProjects(){
        layoutHome.setVisibility(View.GONE);
        rvHomelist.setVisibility(View.GONE);
        layoutscrollPrfoile.setVisibility(View.VISIBLE);

    }
//    private void layoutReport(){
//        mChart = findViewById(R.id.chart1);
////    mChart.getDescription().setText("$4153.93 \\n15TH MAY - 15TH JUNE");
//        mChart.getDescription().setText(getResources().getString(R.string.str_chart_report_desc));
//        mChart.getDescription().setTextSize(12f); //sets the size of the label text in density pixels min = 6f, max = 24f, default is 10f, font size will be in dp
//
//        mChart.getDescription().setTextColor(getResources().getColor(R.color.color_white)); //the color of the font
//
//
//        mChart.getDescription().setTextAlign(Paint.Align.CENTER); //Sets the text alignment of the description text. Default RIGHT
//
////    mChart.getDescription().setPosition(3f,3f);
////        mChart.setBackgroundColor(getResources().getColor(R.color.appbg));
////    mChart.setBackgroundResource(R.drawable.app_rounded_gradient);
////        mChart.setGridBackgroundColor();
//
////        mChart.setDrawGridBackground(true);
////        mChart.setDrawBarShadow(true);
////        mChart.setHighlightFullBarEnabled(true);
//
//        // create marker to display box when values are selected
//        mChart.setTouchEnabled(true);
//        mChart.setOnChartValueSelectedListener(this);
//        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);
//
//        // Set the marker to the chart
//        mv.setChartView(mChart);
//        mChart.setMarker(mv);
//
//
////    mChart.getDescription().setEnabled(false);
//        mChart.getAxisLeft().setDrawGridLines(false);
//        mChart.getXAxis().setDrawGridLines(false);
//        mChart.getXAxis().setDrawAxisLine(false);
//        mChart.setDragEnabled(true);
//        mChart.setScaleEnabled(true);
//        mChart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
//        mChart.getAxisLeft().setEnabled(false);
//        mChart.getAxisRight().setEnabled(false);
//        mChart.getLegend().setEnabled(false);
//
//        mChart.setMaxHighlightDistance(6f);
//
//        LimitLine ll = new LimitLine(100, "Max Capacity");
//        mChart.getAxisLeft().addLimitLine(ll);
//        mChart.setPinchZoom(true);
//        mChart.setDrawBarShadow(true);
//
//
//        mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
//                CombinedChart.DrawOrder.LINE
//        });
//        Legend l = mChart.getLegend();
//        l.setWordWrapEnabled(true);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setForm(Legend.LegendForm.LINE);
//
//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
//
//
//
//        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setDrawGridLines(false);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//
//
//        leftAxis.setDrawAxisLine(false);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.TOP);
////        xAxis.setAxisMinimum(0f);
//
//        xAxis.setGranularityEnabled(true);
//
//        xAxis.setTextSize(9f);
//        xAxis.setTextColor(getResources().getColor(R.color.color_white));
//
//        xAxis.setValueFormatter(new ValueFormatter() {
//            @Override
//            public String getFormattedValue(float value) {
//                return mMonths[(int) value % mMonths.length];
//            }
//        });
//
//
//
//        CombinedData data = new CombinedData();
//
//
//        data.setData(generateLineData());
//
////        xAxis.setAxisMaximum(data.getXMax() + 0.25f);
////        mChart.getXAxis().setAxisMaximum(data.getXMax() + 0.5f);
////        mChart.getXAxis().setAxisMinimum(data.getXMin() - 0.5f);
//        mChart.setViewPortOffsets(18f, 35f, 18f, 35f);
////        mChart.setViewPortOffsets(0f, 0f, 0f, 0f);
//        mChart.notifyDataSetChanged();
//        mChart.setData(data);
//        mChart.invalidate();
//    }

//    private LineData generateLineData() {
//
//
//        ArrayList<Entry> entries = new ArrayList<Entry>();
//        ArrayList<Entry> entries2 = new ArrayList<Entry>();
//
//        entries = getLineEntriesData(entries);
//        entries2 = getLineEntriesDataTwo(entries2);
//
//
//        LineDataSet set = new LineDataSet(entries, "Line");
//        set.setValueFormatter(new PercentFormatter());
//        set.setDrawIcons(false);
//        set.setColor(getResources().getColor(R.color.color_white));
//        set.setLineWidth(4f);
////        set.setCircleColor(getResources().getColor(R.color.color_white));
//        set.setCircleRadius(4f);
//        set.setDrawCircles(true);
////        set.setFillColor(getResources().getColor(R.color.app_front_dark_gry));
////        set.setFillColor(Color.rgb(255,0,0));
////        set.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
//        set.setCircleColor(Color.rgb(85, 130, 245));
//        set.setCircleRadius(4f);
//        set.setFillColor(Color.rgb(85, 130, 245));
//        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set.setDrawValues(false);
//        set.setValueTextSize(10f);
//        set.setDrawIcons(true);
////        set.setValueTextColor(getResources().getColor(R.color.color_white));
//        set.setAxisDependency(YAxis.AxisDependency.LEFT);
//        set.setCubicIntensity(0.2f);
////        set.setDrawFilled(true);
////        set.setFillAlpha(100);
////        set.setDrawHorizontalHighlightIndicator(false);
//
//        LineDataSet set2 = new LineDataSet(entries2, "Line");
//        set2.setColor(Color.rgb(93, 172, 249));
//
//
////        set2.setColors(ColorTemplate.COLORFUL_COLORS);
//        set2.setDrawCircles(false);
//        set2.setLineWidth(1f);
//        set2.setCircleColor(Color.rgb(240, 238, 70));
//        set2.setCircleRadius(5f);
//        set2.setFillColor(Color.rgb(240, 238, 70));
//        set2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set2.setDrawValues(false);
//        set2.setValueTextSize(9f);
//        set2.setValueTextColor(Color.rgb(240, 238, 70));
//
//
//        set2.setAxisDependency(YAxis.AxisDependency.LEFT);
//        LineData d = new LineData(set,set2);
//
//        int maxCapacity = 100;
//
//        mChart.setViewPortOffsets(0f, 0f, 0f, 0f);
////        float xMax = mChart.getData().getDataSetByIndex(0).getXMax();
////        float xMin = 0;
////        mChart.getXAxis().setAxisMaximum(xMax);
////        mChart.getXAxis().setAxisMinimum(xMin);
//        return d;
//
//
//    }
//
//    protected String[] mMonths = new String[]{
//            "JAN", "FEB", "MAR", "APR", "MAY","JUN","JUL","AUG"
////            ,
////            "June","Jul","Aug"
//    };
//
//
//
//    private ArrayList<Entry> getLineEntriesData(ArrayList<Entry> entries2) {
//
//
////        entries2.add(new Entry(0, 20f));
////        entries2.add(new Entry(1, 60f));
////        entries2.add(new Entry(2, 30f));
////        entries2.add(new Entry(3, 10f));
////        entries2.add(new Entry(4, 80f));
////        entries2.add(new Entry(5, 30f));
//
//
//        entries2.add(new Entry(0, 30f));
//        entries2.add(new Entry(1, 60));
//        entries2.add(new Entry(2, 10));
//        entries2.add(new Entry(3, 8));
//        entries2.add(new Entry(4, 40));
//        entries2.add(new Entry(5, 37));
//        entries2.add(new Entry(6, 15f));
//        entries2.add(new Entry(7, 0f));
//
//        return entries2;
//    }
//
//    private ArrayList<Entry> getLineEntriesDataTwo(ArrayList<Entry> entries) {
//        entries.add(new Entry(0, 110f));
//        entries.add(new Entry(1, 110f));
//        entries.add(new Entry(2, 110f));
//        entries.add(new Entry(3, 110f));
//        entries.add(new Entry(4, 110f));
//        entries.add(new Entry(5, 110f));
//        entries.add(new Entry(6, 110f));
//        entries.add(new Entry(7, 110f));
//
//        return entries;
//    }
private void layoutReport(){
    mChart = findViewById(R.id.chart1);
    mChart.getDescription().setEnabled(false);

    mChart.setTouchEnabled(true);
    mChart.setOnChartValueSelectedListener(this);
    MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

    // Set the marker to the chart
    mv.setChartView(mChart);
    mChart.setMarker(mv);


//    mChart.getDescription().setEnabled(false);
    mChart.getAxisLeft().setDrawGridLines(false);
    mChart.getXAxis().setDrawGridLines(false);
    mChart.getXAxis().setDrawAxisLine(false);
    mChart.setDragEnabled(true);
    mChart.setScaleEnabled(true);
    mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    mChart.getAxisLeft().setEnabled(false);
    mChart.getAxisRight().setEnabled(false);
    mChart.getLegend().setEnabled(false);
//        mChart.getXAxis().setAvoidFirstLastClipping(false);
//        mChart.setVisibleXRangeMaximum(5f);
//        mChart.setViewPortOffsets(0f, 0f, 0f, 0f);

    mChart.setMaxHighlightDistance(6f);

    LimitLine ll = new LimitLine(100, "Max Capacity");
    mChart.getAxisLeft().addLimitLine(ll);
    mChart.setPinchZoom(true);
    mChart.setDrawBarShadow(true);
//        mChart.moveViewToX(10);


    // draw bars behind lines
    mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
            CombinedChart.DrawOrder.LINE
    });
//        CombinedChart.DrawOrder.BAR,
    Legend l = mChart.getLegend();
    l.setWordWrapEnabled(true);
    l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
    l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
    l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
    l.setForm(Legend.LegendForm.LINE);

    YAxis rightAxis = mChart.getAxisRight();
    rightAxis.setDrawGridLines(false);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)




    YAxis leftAxis = mChart.getAxisLeft();
    leftAxis.setDrawGridLines(false);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
    leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setAxisMinimum(-4);
//        leftAxis.setSpaceMax(5f);


    leftAxis.setDrawAxisLine(false);
//        leftAxis.setDrawZeroLine(false);
//        leftAxis.setDrawGridLines(false);

    XAxis xAxis = mChart.getXAxis();
    xAxis.setPosition(XAxis.XAxisPosition.TOP);
//        xAxis.setAxisMinimum(0f);

    xAxis.setGranularityEnabled(true);
    xAxis.setEnabled(false);


//        xAxis.setSpaceMin(150f);
//        xAxis.setGranularity(1.0f);
//    xAxis.setTypeface(AppController.tazRegular);
    xAxis.setTextSize(9f);
//    xAxis.setTextColor(getResources().getColor(R.color.color_white));
    xAxis.setTextColor(Color.rgb(93, 172, 249));
    xAxis.setValueFormatter(new ValueFormatter() {
        @Override
        public String getFormattedValue(float value) {
            return mMonths[(int) value % mMonths.length];
        }
    });


    CombinedData data = new CombinedData();


    data.setData(generateLineData());
//     xAxis.setSpaceMax(35f);
//     xAxis.setSpaceMin(5f);
//        data.setData(generateBarData());

//    xAxis.setAxisMaximum(data.getXMax() + 0.25f);
    mChart.getXAxis().setAxisMaximum(data.getXMax() + 0.25f);
    mChart.getXAxis().setAxisMinimum(data.getXMin() - 0.25f);
//    mChart.setViewPortOffsets(18f, 35f, 18f, 35f);
//        xAxis.setAxisMaximum(data.getXMax() + 1f);
//        xAxis.setLabelCount(7);
//        xAxis.setGranularity(0.1f);
    mChart.setData(data);
    mChart.invalidate();






}



    private LineData generateLineData() {


        ArrayList<Entry> entries = new ArrayList<Entry>();

        entries = getLineEntriesData(entries);


        ClosedNetworkApp.Log("entries Arr :"+entries.size());


        LineDataSet set = new LineDataSet(entries, "Line");
        Entry circleEntry = set.getEntryForIndex(set.getEntryCount()-1);

        int size = set.getEntryCount();
        for (int i=0; i<size; i++) {

            if(i == 4){
            set.getEntryForIndex(i).setIcon(ContextCompat.getDrawable(this,R.drawable.drw_chart_circle_indictor_img));

            }


            ClosedNetworkApp.Log("entries Arr i :"+i);


        }



        set.setValueFormatter(new PercentFormatter());
        set.setDrawIcons(true);
        set.setColor(getResources().getColor(R.color.color_white));
        set.setLineWidth(3f);
//        set.setCircleRadius(4f);
        set.setDrawCircles(false);


//        set.setCircleColor(Color.rgb(85, 130, 245));
//        set.setCircleRadius(6f);
        set.setFillColor(Color.rgb(85, 130, 245));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(false);
        set.setValueTextSize(10f);
//        set.setValueTextColor(getResources().getColor(R.color.color_white));
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setCubicIntensity(0.2f);

        LineData d = new LineData(set);

        int maxCapacity = 100;


        return d;


    }

    protected String[] mMonths = new String[]{
            "JAN", "FEB", "MAR", "APR", "MAY","JUN"
//            ,
//            "June","Jul","Aug"
    };



    private ArrayList<Entry> getLineEntriesData(ArrayList<Entry> entries2) {



        entries2.add(new Entry(0, 30f));
        entries2.add(new Entry(1, 40));
        entries2.add(new Entry(2, 14));
        entries2.add(new Entry(3, 20));
        entries2.add(new Entry(4, 60));
        entries2.add(new Entry(5, 37));
        entries2.add(new Entry(6, 15f));

        return entries2;
    }

    private ArrayList<Entry> getLineEntriesDataTwo(ArrayList<Entry> entries) {
        entries.add(new Entry(0, 100f));
        entries.add(new Entry(1, 100f));
        entries.add(new Entry(2, 100f));
        entries.add(new Entry(3, 100f));
        entries.add(new Entry(4, 100f));
        entries.add(new Entry(5, 100f));
//        entries.add(new Entry(6, 0));
//        entries.add(new Entry(7, 0));

        return entries;
    }


    @Override
    public void onClick(View v) {

        if(v == txtChangedPwd){
            Intent intent = new Intent(act, ChangedPassword.class);
            act.startActivity(intent);
        }
      else  if(v == imgDiviendsSetting){
            Intent intent = new Intent(act, Setting.class);
            act.startActivity(intent);
        }
        else  if(v == imgProfileSetting){
            Intent intent = new Intent(act, Setting.class);
            act.startActivity(intent);
        }
       else if(v == imgReportSetting){
            Intent intent = new Intent(act, Setting.class);
            act.startActivity(intent);
        }

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
        //   super.onBackPressed();

    }


    @Override
    public void setAdapter(ArrayList<ArticlesModel> arrayList) {
        decoration = new DividerItemDecoration(Constant.dipToPx(act, 4), 2);
        rvHomelist.addItemDecoration(decoration);
        rvHomelist.setHasFixedSize(true);

        if (arrayList != null) {
            homeListAdapter = new HomeListAdapater(arrayList, act,2);
            rvHomelist.setAdapter(homeListAdapter);
            rvHomelist.setVisibility(View.VISIBLE);
        } else {
            rvHomelist.setVisibility(View.GONE);
        }
        StaggeredGridLayoutManager Layoutmanager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        Layoutmanager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvHomelist.setLayoutManager(Layoutmanager);

    }






}