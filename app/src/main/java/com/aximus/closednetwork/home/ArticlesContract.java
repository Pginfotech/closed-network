package com.aximus.closednetwork.home;

import com.aximus.closednetwork.base.BaseInterface;
import com.aximus.closednetwork.base.MvpPresenter;

import java.util.ArrayList;

public class ArticlesContract {
    interface ArticlesView extends BaseInterface {
        void setAdapter(final ArrayList<ArticlesModel> arrayList);

    }

    interface Presenter extends MvpPresenter<ArticlesView> {
        void callWebService();

    }
}
