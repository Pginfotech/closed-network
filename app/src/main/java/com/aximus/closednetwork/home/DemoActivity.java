package com.aximus.closednetwork.home;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.common.Constant;
import com.aximus.closednetwork.view.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class DemoActivity extends BaseActivity implements ArticlesContract.ArticlesView {



    Activity act;
    RecyclerView rvHomelist;
    public List<HomeListObject> arrthomeList = new ArrayList<HomeListObject>();
    DividerItemDecoration decoration;
    HomeListAdapater homeListAdapter;
    ArticlesPresenterImpl articlesPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        act = this;

        articlesPresenter = new ArticlesPresenterImpl(DemoActivity.this);
        articlesPresenter.onAttach(this);

        initializeComponents();
        addComponent();

        articlesPresenter.callWebService();
    }

    @Override
    public void initializeComponents() {
        rvHomelist = findViewById(R.id.rv_Homelst);
    }

    @Override
    public void addComponent() {

    }

    @Override
    public void setAdapter(ArrayList<ArticlesModel> arrayList) {
        decoration = new DividerItemDecoration(Constant.dipToPx(act, 4), 2);
        rvHomelist.addItemDecoration(decoration);
        rvHomelist.setHasFixedSize(true);

        if (arrayList != null) {
            homeListAdapter = new HomeListAdapater(arrayList, act,2);
            rvHomelist.setAdapter(homeListAdapter);
            rvHomelist.setVisibility(View.VISIBLE);
        } else {
            rvHomelist.setVisibility(View.GONE);
        }
        StaggeredGridLayoutManager Layoutmanager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        Layoutmanager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvHomelist.setLayoutManager(Layoutmanager);

    }



}
