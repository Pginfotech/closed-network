package com.aximus.closednetwork.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.common.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import static com.aximus.closednetwork.common.Constant.nHeight;

public class HomeListAdapater extends
        RecyclerView.Adapter<HomeListAdapater.ViewHolder> {

    private ArrayList<ArticlesModel> arrayList;
    private Activity context;
    ItemClickListener clickListener;
    private float nWidth;
    private int nRowCount;
    private ArrayList<Integer> arrColor = new ArrayList<>();

    public interface ItemClickListener {
        void onItemClick(View view, int position);

    }

    public HomeListAdapater(ArrayList<ArticlesModel> arrayList, Activity ctx, int nRowCount) {
        this.arrayList = arrayList;
        this.context = ctx;
        this.nRowCount = nRowCount;
        if (Constant.nWidth <= 0 || nHeight <= 0) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ctx.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            nHeight = displaymetrics.heightPixels;
            Constant.nWidth = displaymetrics.widthPixels;
        }

        nWidth = (float) (Constant.nWidth) / nRowCount;

        arrColor.add(ContextCompat.getColor(ctx, R.color.random_1));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_2));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_3));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_4));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_5));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_6));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_7));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_8));
        arrColor.add(ContextCompat.getColor(ctx, R.color.random_9));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_home_list, parent, false);

        ViewHolder viewHolder =
                new ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        final int itemPos = position;
//        final HomeListObject object = adsList.get(position);
        ArticlesModel model = arrayList.get(position);
        float nHeight;
        if (model != null) {
            holder.titleTextView.setText(model.articleTitle.trim());
            Log.e("Adpater,,,,,,", ",,,,,,"+model.articleTitle);

            if (nWidth <= 0)
                nWidth = (float) (Constant.nWidth) / nRowCount;
//            nHeight = ((float) object.getImghight() / (float) object.getImgwidth()) * nWidth;

            try {
                Glide.with(context).load(model.articleImage).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }


                })
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).override(((int) nWidth), 160))
                        .into(holder.postImageView);
            } catch (IllegalArgumentException | OutOfMemoryError e) {
//                MyUtils.Log("Home Sub View Adapter : " + e.toString());
            }

        }
        holder.setItemClickListener(new HomeListAdapater.ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent intent = new Intent(context, DemoActivity.class);
                context.startActivity(intent);
//                Intent intent = new Intent(context, HomeDetailsActivity.class);
//                intent.putExtra("url", arrayList.get(position).articleImage);
//                intent.putExtra("desc", arrayList.get(position).articleDescription);
//                intent.putExtra("title", arrayList.get(position).articleTitle);
//                intent.putExtra("datetime", arrayList.get(position).articleDateTime);
//                context.startActivity(intent);
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Context context;
        public ImageView postImageView;
        public TextView titleTextView,txtTestimoniDate,txtTestimoniTime;
        public TextView detailsTextView;

        public ViewHolder(View view) {
            super(view);

            postImageView = view.findViewById(R.id.postImageView);
            titleTextView = view.findViewById(R.id.titleTextView);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onItemClick(v, getAdapterPosition());
        }

        public void setItemClickListener(HomeListAdapater.ItemClickListener ic) {
            clickListener = ic;
        }

    }


}

