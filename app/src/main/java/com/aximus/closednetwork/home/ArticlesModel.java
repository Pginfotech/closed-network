package com.aximus.closednetwork.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
public class ArticlesModel implements Parcelable {

    @SerializedName("ArticleDateTime")
    public String articleDateTime = "";
    @SerializedName("ArticleImage")
    public String articleImage = "";
    @SerializedName("Image")
    public String image = "";
    @SerializedName("ArticleDescription")
    public String articleDescription = "";
    @SerializedName("ArticleTitle")
    public String articleTitle = "";
    @SerializedName("ArticleId")
    public int articleId;

    protected ArticlesModel(Parcel in) {
        articleDateTime = in.readString();
        articleImage = in.readString();
        image = in.readString();
        articleDescription = in.readString();
        articleTitle = in.readString();
        articleId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(articleDateTime);
        dest.writeString(articleImage);
        dest.writeString(image);
        dest.writeString(articleDescription);
        dest.writeString(articleTitle);
        dest.writeInt(articleId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArticlesModel> CREATOR = new Creator<ArticlesModel>() {
        @Override
        public ArticlesModel createFromParcel(Parcel in) {
            return new ArticlesModel(in);
        }

        @Override
        public ArticlesModel[] newArray(int size) {
            return new ArticlesModel[size];
        }
    };
}
