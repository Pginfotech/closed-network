package com.aximus.closednetwork.home;

public class HomeListObject {
    String homeTitale,homeImage;
    int imghight,imgwidth,temImag;

    public HomeListObject() {
    }

    public HomeListObject(String homeTitale, String homeImage, int imghight, int imgwidth,int temImag) {
        this.homeTitale = homeTitale;
        this.homeImage = homeImage;
        this.imghight = imghight;
        this.imgwidth = imgwidth;
        this.temImag = temImag;
    }

    public void setTemImag(int temImag) {
        this.temImag = temImag;
    }

    public int getTemImag() {
        return temImag;
    }

    public void setHomeTitale(String homeTitale) {
        this.homeTitale = homeTitale;
    }

    public void setHomeImage(String homeImage) {
        this.homeImage = homeImage;
    }

    public void setImghight(int imghight) {
        this.imghight = imghight;
    }

    public void setImgwidth(int imgwidth) {
        this.imgwidth = imgwidth;
    }

    public String getHomeTitale() {
        return homeTitale;
    }

    public String getHomeImage() {
        return homeImage;
    }

    public int getImghight() {
        return imghight;
    }

    public int getImgwidth() {
        return imgwidth;
    }
}
