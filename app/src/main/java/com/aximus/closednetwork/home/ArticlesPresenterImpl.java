package com.aximus.closednetwork.home;

import android.content.Context;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BasePresenter;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.webservices.resopnse.ArticlesResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticlesPresenterImpl extends BasePresenter<ArticlesContract.ArticlesView> implements ArticlesContract.Presenter {

    private final Context context;

    public ArticlesPresenterImpl(Context context) {
        this.context = context;
    }

    @Override
    public void handleApiError(String error) {

    }


    @Override
    public void callWebService() {
        if (NetworkUtils.isNetworkConnected(context)) {


            ClosedNetworkApp.getApiClient().getWebServices().callGetArticles().enqueue(new Callback<ArticlesResponse>() {
                @Override
                public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                    ClosedNetworkApp.Log("Get Reponse :"+response.body());

                    if (response != null) {
                        if (response.isSuccessful()) {
                            getMvpView().setAdapter(response.body().articlesModelArrayList);
                        } else {
                            getMvpView().showMessage(response.body().message);
                            getMvpView().setAdapter(null);
                        }
                    } else {
                        getMvpView().showMessage(context.getString(R.string.something_wen_wrong));
                        getMvpView().setAdapter(null);
                    }
                }

                @Override
                public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                    getMvpView().showMessage(t.getMessage());
                    getMvpView().setAdapter(null);
                }
            });

        } else {
            getMvpView().hideLoading();
            getMvpView().onError(context.getString(R.string.no_internet));
            getMvpView().setAdapter(null);
        }

    }


}
