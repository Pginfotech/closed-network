package com.aximus.closednetwork.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.utils.Utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HomeDetailsActivity extends BaseActivity implements View.OnClickListener {

    ImageView imgActionBack;
    Activity act;
    String articleImg,articleTitle,articleDesc,articleDateTime;
    AppCompatTextView txtarticleTitle,txtarticleDesc,txtarticleDate,txtarticleTime;
    ImageView imgArticle;
    LinearLayout layoutImg;
    FrameLayout frameImg;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_details);
        act = this;

//        imgActionBack = findViewById(R.id.img_ActionBack);
//        imgActionBack.setOnClickListener(this);

        if(getIntent()!=null){
            articleImg =  getIntent().getStringExtra("url");
            articleDesc =  getIntent().getStringExtra("desc");
            articleTitle =  getIntent().getStringExtra("title");
            articleDateTime =  getIntent().getStringExtra("datetime");
            ClosedNetworkApp.Log("DEsc :"+articleTitle);
        }
        ClosedNetworkApp.Log("articleTitle :"+articleDesc);

        initializeComponents();
        addComponent();


    }

    @Override
    public void initializeComponents() {

        imgArticle = findViewById(R.id.img_Article);
        imgActionBack = findViewById(R.id.img_ActionBack);
        imgActionBack.setOnClickListener(this);
        txtarticleTitle = findViewById(R.id.txt_Title);
        txtarticleDesc = findViewById(R.id.txt_Desc);
        txtarticleDate = findViewById(R.id.txt_Date);
        txtarticleTime = findViewById(R.id.txt_Time);
//        layoutImg = findViewById(R.id.line_Image);
//        frameImg = findViewById(R.id.frame_Img);





    }

    @Override
    public void addComponent() {
        Glide.with(this).load(articleImg).into(imgArticle);

//        try {
//            URL url = new URL(articleImg);
//            Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//            Drawable image = new BitmapDrawable(act.getResources(), bitmap);
//
//            layoutImg.setBackground(image);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        imgArticle.setBackground(articleImg);
        txtarticleTitle.setText(articleTitle);
        txtarticleDesc.setText(articleDesc);
        if(articleDateTime.length() > 0){
            if(articleDateTime.length() == 19){
                String strDate = articleDateTime.substring(0,10);
                String strTime = articleDateTime.substring(12,19);
                txtarticleDate.setText(getFormatedDate(strDate));
                txtarticleTime.setText(getFormatedTime(strTime,"HH:mm:ss","HH:mm"));
            }
            else {
                txtarticleDate.setText("");
                txtarticleTime.setText("");
            }

        }


    }

    @SuppressLint("SimpleDateFormat")
    public static String getFormatedDate(String dateStr){
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dateString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        System.out.println(dateString);
        return dateString;
    }
    public static String getFormatedTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }


    @Override
    public void onClick(View v) {
        if(v == imgActionBack){
            act.finish();
        }
    }
}
