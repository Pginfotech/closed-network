package com.aximus.closednetwork.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.common.Constant;
import com.aximus.closednetwork.view.DividerItemDecoration;
import com.github.mikephil.charting.charts.CombinedChart;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private View rootView;

    DividerItemDecoration decoration;
    HomeListAdapater homeListAdapter;
    RecyclerView rvHomelist;
    public  List<HomeListObject> arrthomeList = new ArrayList<HomeListObject>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.layout_home, container, false);
        setUpRecyclerView();
        return rootView;
    }

    private void setUpRecyclerView() {

//        Log.e("Tagg,,,,,","lll");
//        rvHomelist = rootView.findViewById(R.id.recyclerView);
//        decoration = new DividerItemDecoration(Constant.dipToPx(getActivity(), 4), 2);
//        rvHomelist.addItemDecoration(decoration);
//        Log.e("Tagg 2,,,,,","lll");
//        rvHomelist.setHasFixedSize(true);
//
//        homeListAdapter = new HomeListAdapater(arrthomeList,getActivity(),2);
//        rvHomelist.setAdapter(homeListAdapter);
//        Log.e("Tagg,,,,,3 ","lll");
////        rvHomelist.addOnScrollListener(getOnBottomListener((StaggeredGridLayoutManager) rvHomelist.getLayoutManager()));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//
//
//        StaggeredGridLayoutManager Layoutmanager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
//        Layoutmanager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
//        rvHomelist.setLayoutManager(Layoutmanager);

    }
//    RecyclerView.OnScrollListener getOnBottomListener(final StaggeredGridLayoutManager layoutManager) {
//        return new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView rv, int dx, int dy) {
//                Log.e("Tagg,,,,,4 ","lll");
//                if (arrthomeList != null && arrthomeList.size() > 0) {
//                    boolean isBottom = layoutManager.findLastVisibleItemPositions(new int[3])[1] >= homeListAdapter.getItemCount() - 3;
//                    Log.e("Tag ;;",""+(layoutManager.findLastCompletelyVisibleItemPositions(new int[3])[1]) + " : " + (homeListAdapter.getItemCount() - 3));
//                    if (isBottom) {
//                        arrthomeList.add(new HomeListObject("New technology has been introduced","",551,834,R.drawable.ic_homelist_one));
//                        arrthomeList.add(new HomeListObject("New application will change the world","",355,536,R.drawable.ic_homelist_two));
//                        arrthomeList.add(new HomeListObject("New application will change the world","",359,539,R.drawable.ic_homelist_three));
//                        arrthomeList.add(new HomeListObject("New technology has been introduced","",423,606,R.drawable.ic_homelist_four));
//
//                        homeListAdapter = new HomeListAdapater(arrthomeList,getActivity(),2);
//                        rvHomelist.setAdapter(homeListAdapter);
//                    }
//                }
//            }
//        };
//    }
    private List<String> dummyStrings() {
        List<String> colorList = new ArrayList<>();
        colorList.add("#354045");
        colorList.add("#20995E");
        colorList.add("#76FF03");
        colorList.add("#E26D1B");
        colorList.add("#911717");
        colorList.add("#9C27B0");
        colorList.add("#FFC107");
        colorList.add("#01579B");
        return colorList;
    }
}