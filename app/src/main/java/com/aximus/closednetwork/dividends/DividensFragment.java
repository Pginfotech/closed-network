package com.aximus.closednetwork.dividends;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.aximus.closednetwork.R;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.model.GradientColor;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.List;

public class DividensFragment    extends Fragment {

    private View rootView;
    private CombinedChart mChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.layout_dividends, container, false);


        mChart = rootView.findViewById(R.id.chart1);
        mChart.getDescription().setText("");
        mChart.setBackgroundColor(getResources().getColor(R.color.app_theme_color));
//        mChart.setDrawGridBackground(true);
//        mChart.setDrawBarShadow(true);
        mChart.setHighlightFullBarEnabled(true);


        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.getAxisLeft().setDrawLabels(false);
        mChart.getAxisRight().setDrawLabels(false);

        mChart.getDescription().setEnabled(false);

        // draw bars behind lines
//        mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
//                CombinedChart.DrawOrder.BAR,  CombinedChart.DrawOrder.LINE
//        });
        mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.BAR
        });

        Legend l = mChart.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        mChart.getLegend().setEnabled(false);



        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setEnabled(false);



        ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.add("JAN");
        xAxisLabel.add("FEB");
        xAxisLabel.add("MAR");
        xAxisLabel.add("APR");
        xAxisLabel.add("MAY");
        xAxisLabel.add("JUN");
        xAxisLabel.add("JUL");

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(getResources().getColor(R.color.color_white));
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return mMonths[(int) value % mMonths.length];
//            }
//        });

        CombinedData data = new CombinedData();

//        data.setData( generateLineData());
        data.setData(generateBarData());

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);
        mChart.setData(data);
        mChart.invalidate();
        return rootView;
    }


    private ArrayList<Entry> getLineEntriesData(ArrayList<Entry> entries){
        entries.add(new Entry(1, 20));
        entries.add(new Entry(2, 10));
        entries.add(new Entry(3, 8));
        entries.add(new Entry(4, 40));
        entries.add(new Entry(5, 37));
//        entries.add(new Entry(6, 37));

        return entries;
    }

    private BarData generateBarData() {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        entries = getBarEnteries(entries);

        BarDataSet set1 = new BarDataSet(entries, "");
        //set1.setColor(Color.rgb(60, 220, 78));
//        Paint mPaint = mChart.getRenderer().getPaintRender(); mPaint.setShader(new
//                SweepGradient(350,120,Color.parseColor("#FFF212"),Color.parseColor("#FCE121")));

        int startColor1 = ContextCompat.getColor(getActivity(), android.R.color.holo_orange_light);
        int startColor2 = ContextCompat.getColor(getActivity(), android.R.color.holo_blue_light);
        int startColor3 = ContextCompat.getColor(getActivity(), android.R.color.holo_orange_light);
        int startColor4 = ContextCompat.getColor(getActivity(), android.R.color.holo_green_light);
        int startColor5 = ContextCompat.getColor(getActivity(), android.R.color.holo_red_light);
        int endColor1 = ContextCompat.getColor(getActivity(), android.R.color.holo_blue_dark);
        int endColor2 = ContextCompat.getColor(getActivity(), android.R.color.holo_purple);
        int endColor3 = ContextCompat.getColor(getActivity(), android.R.color.holo_green_dark);
        int endColor4 = ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark);
        int endColor5 = ContextCompat.getColor(getActivity(), android.R.color.holo_orange_dark);

        List<GradientColor> gradientColors = new ArrayList<>();
        gradientColors.add(new GradientColor(startColor1, endColor1));
        gradientColors.add(new GradientColor(startColor2, endColor2));
        gradientColors.add(new GradientColor(startColor3, endColor3));
        gradientColors.add(new GradientColor(startColor4, endColor4));
        gradientColors.add(new GradientColor(startColor5, endColor5));
        gradientColors.add(new GradientColor(startColor2, endColor2));

        set1.setGradientColors(gradientColors);


        set1.setDrawIcons(false);
//        set1.setColors(ColorTemplate.JOYFUL_COLORS);
        set1.setValueTextColor(getResources().getColor(R.color.color_white));
        set1.setValueTextSize(10f);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//        set1.setBarBorderWidth(50f);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        float barWidth = 0.12f; // x2 dataset


        BarData d = new BarData(set1);
        d.setBarWidth(barWidth);


        return d;
    }

    private ArrayList<BarEntry> getBarEnteries(ArrayList<BarEntry> entries){
        entries.add(new BarEntry(0, 25));
        entries.add(new BarEntry(1, 25));
        entries.add(new BarEntry(2, 30));
        entries.add(new BarEntry(3, 38));
        entries.add(new BarEntry(4, 10));
        entries.add(new BarEntry(5, 15));
        entries.add(new BarEntry(6, 20));
        return  entries;
    }
}