package com.aximus.closednetwork.signin;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BasePresenter;
import com.aximus.closednetwork.signup.SignUp;
import com.aximus.closednetwork.util.CommonSharedPreferences;
import com.aximus.closednetwork.util.CommonUtils;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.webservices.WebParam;
import com.aximus.closednetwork.webservices.resopnse.LoginResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter  extends BasePresenter<LoginContract.LoginView> implements LoginContract.Presenter {

    private final Context context;
    private SignIn activity;

    public LoginPresenter(Context context) {
        this.context = context;
        activity = (SignIn) context;
    }


    @Override
    public void handleApiError(String error) {

    }

    @Override
    public boolean checkValidation() {
        if (TextUtils.isEmpty(activity.edtEmail.getText().toString().trim())) {
            getMvpView().onError(activity.getString(R.string.error_email));
            return false;
        } else if (!CommonUtils.isValidEmail(activity.edtEmail.getText().toString().trim())) {
            getMvpView().onError(activity.getString(R.string.error_email));
            return false;
        } else if (TextUtils.isEmpty(activity.edtPassword.getText().toString().trim())) {
            getMvpView().onError(activity.getString(R.string.error_password));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void callLoginWS() {
        if (NetworkUtils.isNetworkConnected(context)) {
            ClosedNetworkApp.showProgress(activity, activity.getResources().getString(R.string.str_loader));
            String Email = activity.edtEmail.getText().toString().trim();
            String Password = activity.edtPassword.getText().toString().trim();

            ClosedNetworkApp.getApiClient().getWebServices().callTryLogin(Email,Password).enqueue(new Callback<LoginResponse>() {

                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    ClosedNetworkApp.dismissProgress();

                    ClosedNetworkApp.Log("app Login :"+response.body());
                    if (response.body().isSuccess) {
                        LoginResponse loginModel = new LoginResponse();
                        loginModel.userName = response.body().userName;
                        ClosedNetworkApp.Log("userName Login :"+ response.body().userName);
                        loginModel.userEmaill = response.body().userEmaill;
                        loginModel.userPhone1 = response.body().userPhone1;
                        loginModel.userPhone2 = response.body().userPhone2;
                        loginModel.userPassword = response.body().userPassword;
                        loginModel.userAddress = response.body().userAddress;
                        loginModel.userCity = response.body().userCity;
                        loginModel.userCountry = response.body().userCountry;
                        loginModel.userId = response.body().userId;
                        ClosedNetworkApp.Log("userPassword Login :"+ response.body().userPassword);
                        CommonSharedPreferences sharedPreferences = new CommonSharedPreferences(activity);
                        sharedPreferences.setUpSharedPreferences();
                        sharedPreferences.setPreferenceData(loginModel);

//                        getMvpView().showMessage(response.body().message);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.openHomeScreen();
                            }
                        }, 1000);
////
                    } else {
                        getMvpView().onError(response.body().message);
                    }
                }



                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    ClosedNetworkApp.dismissProgress();
                    getMvpView().onError(t.getMessage());
                }
            });
        } else {
            ClosedNetworkApp.dismissProgress();
            getMvpView().onError(context.getString(R.string.no_internet));

        }
    }
}
