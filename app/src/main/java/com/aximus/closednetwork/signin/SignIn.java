package com.aximus.closednetwork.signin;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.changpwd.ChangedPassword;
import com.aximus.closednetwork.forgetpwd.ForgotPassword;
import com.aximus.closednetwork.home.Home;
import com.aximus.closednetwork.report.Report;
import com.aximus.closednetwork.signup.SignUp;

public class SignIn  extends BaseActivity implements LoginContract.LoginView, View.OnClickListener {

    Activity act;
    AppCompatTextView txtChangePwd,txtSignIn;

    private LoginPresenter loginPresenter;
    public EditText edtEmail;
    public EditText edtPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sign_in);
        act = this;

        loginPresenter = new LoginPresenter(SignIn.this);
        loginPresenter.onAttach(SignIn.this);
        initializeComponents();
        addComponent();



    }

    @Override
    public void initializeComponents() {
        txtChangePwd = findViewById(R.id.txt_ChangePwd);
        txtSignIn = findViewById(R.id.txt_SignIn);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);

    }

    @Override
    public void addComponent() {

        txtChangePwd.setOnClickListener(this);
        txtSignIn.setOnClickListener(this);

        edtPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    txtSignIn.performClick();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onDestroy() {
        loginPresenter.onDetach();
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
//        applicationClosedDialog();
        finishAffinity();
        System.exit(0);
    }
    @Override
    public void onClick(View v) {
        hideKeyboard();
        if(v == txtChangePwd){
            Intent intent = new Intent(act, ForgotPassword.class);
            act.startActivity(intent);
        }

        if(v == txtSignIn){
//            Intent intent = new Intent(act, Home.class);
//            act.startActivity(intent);
            if (loginPresenter.checkValidation()) {
                loginPresenter.callLoginWS();
            }
        }
    }

    @Override
    public void openSignUpScreen() {
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
    }

    @Override
    public void openHomeScreen() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    @Override
    public void openForgotPassword() {

    }
}
