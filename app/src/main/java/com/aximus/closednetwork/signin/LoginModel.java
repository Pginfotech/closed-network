package com.aximus.closednetwork.signin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LoginModel implements Parcelable {

//    {"UserId":2,"Name":null,"Email":"b@test.com","PhoneNumber1":"9428116824","PhoneNumber2":null,"Password":"","Country":null,"City":null,"Address":null,"Message":null,"IsSuccess":true}
//    @SerializedName("Name")
    public String userName = "";
//    @SerializedName("Email")
    public String userEmaill = "";
//    @SerializedName("PhoneNumber1")
    public String userPhone1 = "";
//    @SerializedName("PhoneNumber2")
    public String userPhone2 = "";
//    @SerializedName("Password")
    public String userPassword = "";
//    @SerializedName("Country")
    public String userCountry = "";
//    @SerializedName("City")
    public String userCity = "";
//    @SerializedName("Address")
    public String userAddress = "";
//    @SerializedName("UserId")
    public int userId;


    public LoginModel() {
    }

    protected LoginModel(Parcel in) {
        userName = in.readString();
        userEmaill = in.readString();
        userPhone1 = in.readString();
        userPhone2 = in.readString();
        userPassword = in.readString();
        userCountry = in.readString();
        userCity = in.readString();
        userAddress = in.readString();
        userId = in.readInt();
    }

    public static final Creator<LoginModel> CREATOR = new Creator<LoginModel>() {
        @Override
        public LoginModel createFromParcel(Parcel in) {
            return new LoginModel(in);
        }

        @Override
        public LoginModel[] newArray(int size) {
            return new LoginModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(userEmaill);
        dest.writeString(userPhone1);
        dest.writeString(userPhone2);
        dest.writeString(userPassword);
        dest.writeString(userCountry);
        dest.writeString(userCity);
        dest.writeString(userAddress);
        dest.writeInt(userId);
    }
}
