package com.aximus.closednetwork.signin;

import com.aximus.closednetwork.base.BaseInterface;
import com.aximus.closednetwork.base.MvpPresenter;

public interface LoginContract {
    interface LoginView extends BaseInterface {
        void openSignUpScreen();
        void openHomeScreen();
        void openForgotPassword();
    }

    interface Presenter extends MvpPresenter<LoginView> {
        boolean checkValidation();
        void callLoginWS();

    }

}
