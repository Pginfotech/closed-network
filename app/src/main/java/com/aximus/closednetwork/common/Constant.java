package com.aximus.closednetwork.common;

import android.app.Activity;
import android.util.TypedValue;

public class Constant {
    public static int nHeight, nWidth;


    public static int dipToPx(Activity act, int dp) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, act.getResources().getDisplayMetrics());
        return px;
    }
}
