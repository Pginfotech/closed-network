package com.aximus.closednetwork.signup;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BasePresenter;
import com.aximus.closednetwork.util.CommonUtils;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.webservices.resopnse.RegistrationResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter  extends BasePresenter<SignUpContract.View> implements SignUpContract.Presenter {

    private final Context context;
    private SignUp activity;

    public SignUpPresenter(Context context) {
        this.context = context;
        activity = (SignUp) context;

    }

    @Override
    public void handleApiError(String error) {}

    @Override
    public boolean checkValidation() {
        if (TextUtils.isEmpty(activity.edtEmail.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_email));
            activity.edtEmail.requestFocus();
            return false;
        }
        else if (!CommonUtils.isValidEmail(activity.edtEmail.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_email));
            return false;
        }
        else if (TextUtils.isEmpty(activity.edtPhoneNo.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_email));
            return false;
        }
        else if (TextUtils.isEmpty(activity.edtAboutUs.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_email));
            activity.edtPhoneNo.requestFocus();
            return false;
        }
        else if (TextUtils.isEmpty(activity.edtPassword.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_email));
            return false;
        } else if (TextUtils.isEmpty(activity.edtCode.getText().toString().trim())) {
            getMvpView().onError(context.getString(R.string.error_password));
            return false;
        }

        else {
            return true;
        }

    }

    @Override
    public void callRegistrationWS() {
        if (NetworkUtils.isNetworkConnected(context)) {
            ClosedNetworkApp.showProgress(activity, context.getResources().getString(R.string.str_loader));

            String Email = activity.edtEmail.getText().toString().trim();
            String PhoneNumber = activity.edtPhoneNo.getText().toString().trim();
            String HearUs = activity.edtAboutUs.getText().toString().trim();
            String WhyJoinUs = activity.edtJoinUs.getText().toString().trim();
            String AccessCode = activity.edtCode.getText().toString().trim();
            String Password = activity.edtPassword.getText().toString().trim();

            ClosedNetworkApp.getApiClient().getWebServices().callRegistration(Email,PhoneNumber,HearUs,WhyJoinUs,AccessCode,Password,"null").enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    ClosedNetworkApp.dismissProgress();
                    if (response.body().isSuccess) {
                        getMvpView().showMessage(response.body().message);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.openSignUpScreen();
                            }
                        }, 1000);
//
                    } else {
                        getMvpView().onError(response.body().message);
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    ClosedNetworkApp.dismissProgress();
                    getMvpView().onError(t.getMessage());
                }
            });
        } else {
            ClosedNetworkApp.dismissProgress();
            getMvpView().onError(context.getString(R.string.no_internet));
        }
    }



}
