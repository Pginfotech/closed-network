package com.aximus.closednetwork.signup;

import com.aximus.closednetwork.base.BaseInterface;
import com.aximus.closednetwork.base.MvpPresenter;

public interface SignUpContract {
    interface View extends BaseInterface {
        void openSignUpScreen();

        void openHomeScreen();
    }


    interface Presenter extends MvpPresenter<View> {

        boolean checkValidation();

        void callRegistrationWS();
//        void callCountryWS();
    }
}
