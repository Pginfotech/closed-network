package com.aximus.closednetwork.signup;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.signin.LoginContract;
import com.aximus.closednetwork.signin.LoginPresenter;

public class SignUp extends BaseActivity implements SignUpContract.View, View.OnClickListener {

    Activity activity;
    AppCompatTextView txtSignUp;
    private SignUpPresenter signUpPresenter;
    public EditText edtPassword,edtEmail,edtPhoneNo,edtAboutUs,edtJoinUs,edtCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sign_up);
        activity = this;
        signUpPresenter = new SignUpPresenter(SignUp.this);
        signUpPresenter.onAttach(SignUp.this);
        initializeComponents();
        addComponent();
    }

    @Override
    public void initializeComponents() {

        txtSignUp = findViewById(R.id.txt_SignUp);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        edtPhoneNo = findViewById(R.id.edt_userPhoneNo);
        edtAboutUs = findViewById(R.id.edt_userAboutus);
        edtJoinUs = findViewById(R.id.edt_userJoinus);
        edtCode = findViewById(R.id.edt_userMode);
        edtCode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    txtSignUp.performClick();
                    return true;
                }
                return false;
            }
        });



    }
    @Override
    protected void onDestroy() {
        signUpPresenter.onDetach();
        super.onDestroy();
    }
    @Override
    public void addComponent() {
        txtSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if(v == txtSignUp){
//            Intent intent = new Intent(act, Home.class);
//            act.startActivity(intent);
            if (signUpPresenter.checkValidation()) {
                signUpPresenter.callRegistrationWS();
            }
        }
    }

    @Override
    public void openSignUpScreen() {

    }

    @Override
    public void openHomeScreen() {

    }
//
//    @Override
//    public void openForgotPassword() {
//
//    }
}
