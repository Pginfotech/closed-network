package com.aximus.closednetwork.pitch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.view.CenteredToolbar;

public class Pitch extends AppCompatActivity implements View.OnClickListener {

    Activity act;
//    ImageView imgActionBack;
    private CenteredToolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pitch);
        act = this;
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Pitch");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(act);
                onBackPressed();
            }
        });
//
//        imgActionBack = findViewById(R.id.img_ActionBack);
//        imgActionBack.setOnClickListener(this);



    }


    @Override
    public void onClick(View v) {
//        if(v == imgActionBack){
//            hideSoftKeyboard(act);
//            act.finish();
//        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }
}