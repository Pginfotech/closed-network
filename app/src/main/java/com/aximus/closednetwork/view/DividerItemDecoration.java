package com.aximus.closednetwork.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;
    private final int nRowCount;

    public DividerItemDecoration(int space, int nRowCount) {
        this.mSpace = space;
        this.nRowCount = nRowCount;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace / 2;
        outRect.right = mSpace / 2;
        outRect.bottom = mSpace;
        outRect.top = 0;

        if (parent.getChildAdapterPosition(view) < nRowCount)
            outRect.top = mSpace;
    }
}