package com.aximus.closednetwork.forgetpwd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.changpwd.ChangedPassword;
import com.aximus.closednetwork.util.CommonUtils;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.webservices.resopnse.ForgotResponseResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword  extends BaseActivity {

    private AppCompatEditText edtEmaill;
    private AppCompatTextView txtSubmit;
    Activity activity;
    String strRequestTime;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot_password);
        activity = this;
        initializeComponents();
        addComponent();


    }
    @SuppressLint("SimpleDateFormat")
    @Override
    public void initializeComponents() {
        txtSubmit = findViewById(R.id.txt_SentEmaill);
        edtEmaill = findViewById(R.id.edt_useEmaill);
        Date today = new Date();
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        strRequestTime = format.format(today);
        System.out.println(strRequestTime);

    }

    @Override
    public void addComponent() {
        edtEmaill.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                    txtSubmit.performClick();
                    return true;
                }
                return false;
            }
        });
        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                if(isValidation()){
                    callForgotPasswordWS();
                }
            }
        });

    }


    private boolean isValidation() {
        if (TextUtils.isEmpty(edtEmaill.getText().toString().trim())) {
            onError(activity.getString(R.string.error_email));
            return false;
        } else if (!CommonUtils.isValidEmail(edtEmaill.getText().toString().trim())) {
            onError(activity.getString(R.string.error_email));
            return false;
        } else {
            return true;
        }

    }
    public void callForgotPasswordWS() {
        if (NetworkUtils.isNetworkConnected(activity)) {
            ClosedNetworkApp.showProgress(activity, activity.getResources().getString(R.string.str_loader));
            ClosedNetworkApp.getApiClient().getWebServices().callForgotPassword(edtEmaill.getText().toString().trim(),strRequestTime).enqueue(new Callback<ForgotResponseResponse>() {
                @Override
                public void onResponse(Call<ForgotResponseResponse> call, Response<ForgotResponseResponse> response) {
                    ClosedNetworkApp.dismissProgress();
                    if (response != null) {
                        if (response.body().isSuccess) {
                            Intent intent = new Intent(activity, ChangedPassword.class);
                            intent.putExtra("code",response.body().code);
                            intent.putExtra("email",edtEmaill.getText().toString().trim());
                            startActivity(intent);

                        } else {
                            onError(response.body().message);
                        }
                    } else {
                        onError(activity.getString(R.string.something_wen_wrong));
                    }
                }

                @Override
                public void onFailure(Call<ForgotResponseResponse> call, Throwable t) {
                    ClosedNetworkApp.dismissProgress();
                    onError(t.getMessage());
                }
            });
        } else {
            ClosedNetworkApp.dismissProgress();
            onError(activity.getString(R.string.no_internet));
        }

    }
}
