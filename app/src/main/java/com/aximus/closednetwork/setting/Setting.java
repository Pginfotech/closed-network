package com.aximus.closednetwork.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

import com.aximus.closednetwork.R;
import com.aximus.closednetwork.contactus.ContactUs;
import com.aximus.closednetwork.pitch.Pitch;
import com.aximus.closednetwork.signup.SignUp;

public class Setting extends AppCompatActivity implements View.OnClickListener {

    Activity act;
    LinearLayout layoutSubscription,layoutSignout,layoutNotification,layoutWork,layoutProject,layoutContactus,layoutAboutus,layoutLanaguage,layoutRisk,layoutCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setting);
        act = this;


        layoutSubscription = findViewById(R.id.layout_Subscription);
        layoutNotification = findViewById(R.id.layout_Notifications);
        layoutSignout = findViewById(R.id.layout_SignOut);
        layoutWork = findViewById(R.id.layout_Works);
        layoutProject = findViewById(R.id.layout_Project);
        layoutContactus = findViewById(R.id.layout_Contact);
        layoutAboutus = findViewById(R.id.layout_About);
        layoutLanaguage = findViewById(R.id.layout_Language);
        layoutRisk = findViewById(R.id.layout_Risk);
        layoutCompany = findViewById(R.id.layout_Company);


        layoutSubscription.setOnClickListener(this);
        layoutNotification.setOnClickListener(this);
        layoutSignout.setOnClickListener(this);
        layoutWork.setOnClickListener(this);
        layoutProject.setOnClickListener(this);
        layoutContactus.setOnClickListener(this);
        layoutAboutus.setOnClickListener(this);
        layoutLanaguage.setOnClickListener(this);
        layoutRisk.setOnClickListener(this);
        layoutCompany.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {
        if(v == layoutSubscription){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
       else if(v == layoutAboutus){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
        else if(v == layoutContactus){
            Intent intent = new Intent(act, ContactUs.class);
            act.startActivity(intent);
        }
        else if(v == layoutLanaguage){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
        else if(v == layoutNotification){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
        else if(v == layoutProject){
            Intent intent = new Intent(act, Pitch.class);
            act.startActivity(intent);
        }
        else if(v == layoutRisk){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
        else if(v == layoutCompany){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
        else if(v == layoutSignout){
            Intent intent = new Intent(act, SignUp.class);
            act.startActivity(intent);
        }
        else if(v == layoutWork){
//            Intent intent = new Intent(act, ForgotPassword.class);
//            act.startActivity(intent);
        }
    }
}
