package com.aximus.closednetwork.changpwd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.aximus.closednetwork.ClosedNetworkApp;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.base.BaseActivity;
import com.aximus.closednetwork.signin.LoginModel;
import com.aximus.closednetwork.util.CommonSharedPreferences;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.webservices.resopnse.LoginResponse;
import com.aximus.closednetwork.webservices.resopnse.RegistrationResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangedPassword extends BaseActivity {

    private AppCompatEditText edtEmaill;
    private AppCompatEditText edtNewPassowrd;
    private AppCompatTextView txtSubmit;
    private Activity context;
    private String code = "";
    private String email ="";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_changed_password);
        context = this;
        if (getIntent() != null) {
            code = getIntent().getStringExtra("code");
            email = getIntent().getStringExtra("email");
        }
        else {
            ClosedNetworkApp.Log("Emaill1 :"+email);
            email = "";
        }
        ClosedNetworkApp.Log("Emaill :"+email);

        initializeComponents();
        addComponent();
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();
        super.onBackPressed();

    }
    @Override
    public void initializeComponents() {


        edtEmaill = findViewById(R.id.edt_userEmaill);
        edtNewPassowrd = findViewById(R.id.edt_userNewPwd);
        txtSubmit = findViewById(R.id.txt_Confirm);
//        txtSubmit.setOnClickListener(this);

    }

    @Override
    public void addComponent() {


        edtNewPassowrd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    txtSubmit.performClick();
                    return true;
                }
                return false;
            }
        });
        txtSubmit.setOnClickListener(view -> {
            hideKeyboard();
            if (isValidation()) {
                callChangePasswordWS();
            }
        });






    }

    private boolean isValidation() {
        CommonSharedPreferences sharedPreferences = new CommonSharedPreferences(ChangedPassword.this);
        LoginResponse model = sharedPreferences.getPreferenceData();
        ClosedNetworkApp.Log("Pwd :"+model.userPassword);
        ClosedNetworkApp.Log("userEmaill :"+model.userEmaill);
        if (TextUtils.isEmpty(model.userPassword)) {
            onError(getString(R.string.error_ch_current_password));
            return false;
        } else if (TextUtils.isEmpty(edtNewPassowrd.getText().toString().trim())) {
            onError(getString(R.string.error_new_password));
            return false;
        } else if (TextUtils.isEmpty(model.userPassword)) {
            onError(getString(R.string.error_confirm_password));
            return false;
        } else if (!edtNewPassowrd.getText().toString().trim().equalsIgnoreCase(model.userPassword)) {
            onError(getString(R.string.error_confirm_new_password));
            return false;
        } else {
            return true;
        }
    }



    public void callChangePasswordWS() {
        if (NetworkUtils.isNetworkConnected(context)) {
            ClosedNetworkApp.showProgress(context, context.getResources().getString(R.string.str_loader));
            CommonSharedPreferences sharedPreferences = new CommonSharedPreferences(ChangedPassword.this);
            LoginResponse model = sharedPreferences.getPreferenceData();
           String NewPassword = edtNewPassowrd.getText().toString().trim();
           String OldPassword =  model.userPassword;
           if(email.length()== 0){
               email = model.userEmaill;
           }

            ClosedNetworkApp.Log("Emaill :"+email);

            ClosedNetworkApp.getApiClient().getWebServices().callChangePassword(email,false,OldPassword,NewPassword).enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    ClosedNetworkApp.dismissProgress();
                    if (response != null) {
                        if (response.body().isSuccess) {
                            showMessage(response.body().message);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 1000);


                        } else {
                            onError(response.body().message);
                        }
                    } else {
                        onError(context.getString(R.string.something_wen_wrong));
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    ClosedNetworkApp.dismissProgress();
                    onError(t.getMessage());
                }
            });
        } else {
            ClosedNetworkApp.dismissProgress();
            onError(context.getString(R.string.no_internet));
        }

    }


}
