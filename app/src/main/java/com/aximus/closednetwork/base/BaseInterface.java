package com.aximus.closednetwork.base;

import android.support.annotation.StringRes;

public interface BaseInterface {

    void initializeComponents();
    void addComponent();

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);
    void onWarning(String message);
    void onError(String message);
    void showMessage(String message);
    void showMessage(@StringRes int resId);
    boolean isNetworkConnected();
    void hideKeyboard();
}
