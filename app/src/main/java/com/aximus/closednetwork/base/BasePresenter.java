package com.aximus.closednetwork.base;

public class BasePresenter <V extends BaseInterface> implements MvpPresenter<V> {

    private static final String TAG = "BasePresenter";
    private V mMvpView;

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    @Override
    public void handleApiError(String error) {

    }

    public V getMvpView() {
        return mMvpView;
    }


}
