package com.aximus.closednetwork.base;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.aximus.closednetwork.R;
import com.aximus.closednetwork.util.NetworkUtils;
import com.aximus.closednetwork.util.ProgressBarHandler;
import com.irozon.sneaker.Sneaker;

import java.util.Locale;

public abstract class BaseActivity extends LocalizationActivity implements BaseInterface {
//    private ProgressBarHandler progressBarHanlder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  StatusBarUtil.setTransparent(this);
    }


    @Override
    public void showLoading() {
//        if (progressBarHanlder == null) {
//            progressBarHanlder = new ProgressBarHandler(this);
//        }
//        progressBarHanlder.show(this);
    }

    @Override
    public void hideLoading() {
//        if (progressBarHanlder != null) {
//            progressBarHanlder.hide();
//        }
    }


    @Override
    public void onError(@StringRes int resId) {
        Sneaker.with(this)
                .setTitle("Error!")
                .setDuration(4000) // Time duration to show
                .autoHide(true) // Auto hide Sneaker view
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setMessage(getString(resId))
                .sneakSuccess();
    }

    @Override
    public void onWarning(String message) {

    }

    @Override
    public void onError(String message) {
        Sneaker.with(this)
                .setTitle("Error!")
                .setDuration(4000) // Time duration to show
                .autoHide(true) // Auto hide Sneaker view
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setMessage(message)
                .sneakError();
    }

    @Override
    public void showMessage(String message) {
        Sneaker.with(this)
                .setTitle("Success!!")
                .setDuration(4000) // Time duration to show
                .autoHide(true) // Auto hide Sneaker view
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setMessage(message)
                .sneakSuccess();
    }

    @Override
    public void showMessage(@StringRes int resId) {

    }

    @Override
    public boolean isNetworkConnected() {
        boolean isNetworkAvailable = NetworkUtils.isNetworkConnected(getApplicationContext());
        return isNetworkAvailable;
    }

    @Override
    public void hideKeyboard() {
//        CommonUtils.hiddenSoftInput(this);
    }


    @Override
    public void finish() {
        super.finish();
    }



    @Override
    public void onResume() {
        super.onResume();

    }


    public abstract void initializeComponents();
}
