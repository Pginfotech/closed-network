package com.aximus.closednetwork.base;

public interface MvpPresenter<V extends BaseInterface>  {
    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(String error);
}
