package com.aximus.closednetwork.webservices.resopnse;

import com.aximus.closednetwork.home.ArticlesModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ArticlesResponse {
    @SerializedName("IsSuccess")
    public boolean isSuccess;
    @SerializedName("Message")
    public String message ="";
    @SerializedName("Articles")
    public ArrayList<ArticlesModel> articlesModelArrayList;

}
