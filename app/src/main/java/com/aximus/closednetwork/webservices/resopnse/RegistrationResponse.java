package com.aximus.closednetwork.webservices.resopnse;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {
    @SerializedName("IsSuccess")
    public boolean isSuccess = false;
    @SerializedName("Message")
    public String message = "";
}
