package com.aximus.closednetwork.webservices.resopnse;

import android.os.Parcel;
import android.os.Parcelable;

import com.aximus.closednetwork.signin.LoginModel;
import com.google.gson.annotations.SerializedName;

public class LoginResponse implements Parcelable {
    @SerializedName("IsSuccess")
    public boolean isSuccess = false;
    @SerializedName("Message")
    public String message = "";
//    @SerializedName("Data")
//    public LoginModel loginModel;

    @SerializedName("Name")
    public String userName = "";
    @SerializedName("Email")
    public String userEmaill = "";
    @SerializedName("PhoneNumber1")
    public String userPhone1 = "";
    @SerializedName("PhoneNumber2")
    public String userPhone2 = "";
    @SerializedName("Password")
    public String userPassword = "";
    @SerializedName("Country")
    public String userCountry = "";
    @SerializedName("City")
    public String userCity = "";
    @SerializedName("Address")
    public String userAddress = "";
    @SerializedName("UserId")
    public int userId;
    public LoginResponse() {
    }
    public LoginResponse(Parcel in) {
        isSuccess = in.readByte() != 0;
        message = in.readString();
        userName = in.readString();
        userEmaill = in.readString();
        userPhone1 = in.readString();
        userPhone2 = in.readString();
        userPassword = in.readString();
        userCountry = in.readString();
        userCity = in.readString();
        userAddress = in.readString();
        userId = in.readInt();
    }

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel in) {
            return new LoginResponse(in);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isSuccess ? 1 : 0));
        dest.writeString(message);
        dest.writeString(userName);
        dest.writeString(userEmaill);
        dest.writeString(userPhone1);
        dest.writeString(userPhone2);
        dest.writeString(userPassword);
        dest.writeString(userCountry);
        dest.writeString(userCity);
        dest.writeString(userAddress);
        dest.writeInt(userId);
    }
}
