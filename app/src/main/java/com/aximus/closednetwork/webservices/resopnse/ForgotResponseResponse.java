package com.aximus.closednetwork.webservices.resopnse;

import com.google.gson.annotations.SerializedName;

public class ForgotResponseResponse {

    @SerializedName("IsSuccess")
    public boolean isSuccess = false;
    @SerializedName("Message")
    public String message = "";
    @SerializedName("ResetCode")
    public String code ="";

}
