package com.aximus.closednetwork.webservices;

import com.aximus.closednetwork.webservices.resopnse.ArticlesResponse;
import com.aximus.closednetwork.webservices.resopnse.ForgotResponseResponse;
import com.aximus.closednetwork.webservices.resopnse.LoginResponse;
import com.aximus.closednetwork.webservices.resopnse.RegistrationResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface WebServices {


    @FormUrlEncoded    // annotation that used with POST type request
   @POST("accountapi/userlogin")
   Call<LoginResponse> callTryLogin(@Field("Email") String userNameValue,
                                 @Field("Password") String passwordValue);



    @FormUrlEncoded
    @POST("accountapi/usersignup")
    Call<RegistrationResponse> callRegistration(@Field("Email") String userEmailValue,
                                                @Field("PhoneNumber1") String userPhoneNoValue,@Field("HearUs") String userHearUsValue,
                                                @Field("WhyJoinUs") String userJoinUsValue,@Field("AccessCode") String userAccessCodeValue,
                                                @Field("Password") String userPwdValue,@Field("UserId") String userIdValue);


    @FormUrlEncoded
    @POST("accountapi/userforgotpassword")
    Call<ForgotResponseResponse> callForgotPassword(@Field("Email") String userEmailValue,@Field("RequestTime") String userRequestTime);
//
    @FormUrlEncoded
    @POST("accountapi/userchangepassword")
    Call<RegistrationResponse> callChangePassword(@Field("Email") String userEmailValue,@Field("IsValidated") boolean userIsValidated,@Field("OldPassword") String userOldPwdValue,@Field("NewPassword") String userNewPwdValue);

    @GET("article/getarticles")
    Call<ArticlesResponse> callGetArticles();




}
